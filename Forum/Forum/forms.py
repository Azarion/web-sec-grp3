from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed, FileRequired
from flask_uploads import configure_uploads, IMAGES, UploadSet, ALL
from wtforms import StringField, PasswordField, SubmitField, BooleanField, ValidationError, HiddenField, FileField, TextAreaField
from wtforms.validators import DataRequired, Length, Email, EqualTo
from Forum.models import User
from Forum import app


images = UploadSet('images', ALL)
configure_uploads(app, images)


class RegistrationForm(FlaskForm):
    username = StringField('Username',
                            validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    password = PasswordField('Password',
                        validators=[DataRequired(), Length(min=4, max=72)])
    confirm_password = PasswordField('Confirm Password',
                        validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Sign up')


    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError('Username already exists.')
    
    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError('Email already exists.')


class LoginForm(FlaskForm):
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    password = PasswordField('Password',
                        validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Login')

class AddFriendForm(FlaskForm):
    submit = SubmitField('Add Friend')

class RemoveFriendForm(FlaskForm):
    submit = SubmitField('Remove Friend')

class UploadForm(FlaskForm):
    image = FileField('image', validators=[FileRequired(),
                                FileAllowed(images, 'Images only!')
                        ])
    title = StringField('Title', validators=[DataRequired(), Length(min=2, max=50)])
    description  = TextAreaField(u'Image Description')
    submit = SubmitField('Post')

class CommentForm(FlaskForm):
    comment = TextAreaField(u'Image Description')
    submit = SubmitField('Comment')
