"""
Routes and views for the flask application.
"""

from datetime import datetime
from flask import Flask, render_template, request, redirect, url_for, flash
from Forum import app, db, bcrypt
from Forum.forms import RegistrationForm, LoginForm, AddFriendForm, RemoveFriendForm, UploadForm, images, CommentForm
from Forum.models import User, Post, Comment
from flask_login import login_user, current_user, logout_user, login_required




@app.route('/')
@app.route('/home')
def home():
    """Renders the home page."""
    return render_template(
        'index.html',
        title='Home Page',
        year=datetime.now().year,
    )

@app.route('/contact')
def contact():
    return render_template(
        'contact.html',
        title='Contact',
        year=datetime.now().year,
        message='Your contact page.'
    )

@app.route('/about')
def about():
    return render_template(
        'about.html',
        title='About',
        year=datetime.now().year,
        message='Your application description page.'
    )

@app.route('/friends', methods=['GET', 'POST'])
@login_required
def friends():
    form = AddFriendForm()
    form1 = RemoveFriendForm()
    friend_list = []

    for friend in current_user.friend:
        friend_list.append(friend.id)
    users = User.query.filter(User.id!= current_user.id).all()
    remove_user = []

    for user in users:
        if user.id in friend_list:
            remove_user.append(user)
    for r_user in remove_user:
        users.remove(r_user)

    if form1.validate_on_submit():
        if request.form.get('friend_id'):
            temp1 = request.form.get('friend_id')
            this_user = User.query.filter_by(email=current_user.email).first()
            remove_user = User.query.filter_by(id=temp1).first()
            this_user.friend.remove(remove_user)
            remove_user.friend.remove(this_user)
            db.session.commit()
            return redirect(url_for('friends'))

    if form.validate_on_submit():
        if request.form.get('user_id'):
            temp = request.form.get('user_id')
            add_user = User.query.filter_by(id=temp).first()
            this_user = User.query.filter_by(email=current_user.email).first()   
            this_user.friend.append(add_user)
            add_user.friend.append(this_user)
            db.session.commit()
            return redirect(url_for('friends'))

    return render_template(
        'friends.html',
        title='Venner',
        year=datetime.now().year,
        message='Your application description page.',
        users= users,
        form=form,
        form1=form1
    )

@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('home'))


@app.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
       return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check email and password', 'danger')
    return (render_template('login.html', title='Login', form=form))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
       return redirect(url_for('home'))
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(username=form.username.data, email=form.email.data, password=hashed_password)
        db.session.add(user)
        db.session.commit()
        flash('Your account has been created! You are now able to log in!', 'success')
        return redirect(url_for('login'))
    return (render_template('register.html', title='Register', form=form))


@app.route("/account")
@login_required
def account():
    return render_template('account.html', title='Account')


@app.route("/posts", methods=['GET', 'POST'])
def posts():
    post_wall = Post.query.filter_by(user_id=current_user.id).all()
    for friend in current_user.friend:
        post = Post.query.filter_by(user_id = friend.id).all()
        for x in range(len(post)):
            post_wall.append(post[x])

    form = UploadForm()
    if form.validate_on_submit():
        if form.image.data:
            filename = images.save(form.image.data)
            description = form.description.data
            title = form.title.data
            filepath = '../static/uploads/images/'+filename
            if filename:
                flash('File uploaded successfully!', 'success')
                post = Post(title=title,content=description,author=current_user,imagePath=filepath)
                db.session.add(post)
                db.session.commit()
                return redirect(url_for('posts'))
    form1 = CommentForm()
    if form1.validate_on_submit():
        if request.form.get('post_id'):
            comment = form1.comment.data
            post_id = request.form.get('post_id')
            post = Post.query.filter_by(id=post_id).first()
            post_comment = Comment(comment=comment,post=post,author=current_user)
            db.session.add(post_comment)
            db.session.commit()
            return redirect(url_for('posts'))
    return render_template(
        'posts.html', 
        form=form,
        form1=form1,
        posts = post_wall
        )
