# web-sec-grp3

This is the source code for the UCL Web-security project 2020.

# Build Docker
Run from the location of the __dockerfile__  
`sudo docker build -t web-sec3:latest .`

# Run container

`sudo docker run --rm -p 80:8080 web-sec3`

# Connect

Connect via localhost:80