FROM python:3

WORKDIR /app

COPY ./requirements.txt /app/requirements.txt

RUN pip3 install -r requirements.txt

COPY . /app

WORKDIR /app/Forum

CMD [ "python3", "runserver.py" ]
